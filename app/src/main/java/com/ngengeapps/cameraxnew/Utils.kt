package com.ngengeapps.cameraxnew

import android.content.ContentValues
import android.content.Context
import android.media.MediaCodec
import android.media.MediaExtractor
import android.media.MediaMuxer
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.camera.video.MediaStoreOutputOptions
import java.nio.ByteBuffer
import java.text.SimpleDateFormat
import java.util.Locale

object Utils {

    private const val FILENAME_FORMAT = "yyyyMMdd_HHmmss"
    fun createContentValues(isImage: Boolean = true, suffix: String = ""): ContentValues {

        val mimeType = if (isImage) "image/jpeg" else "video/mp4"
        val app = "CameraX"
        val relativePath = if (isImage) "Pictures/$app" else "Movies/$app"
        val name = SimpleDateFormat(FILENAME_FORMAT, Locale.US)
            .format(System.currentTimeMillis())
        return ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, "$name$suffix")
            put(MediaStore.MediaColumns.MIME_TYPE, mimeType)
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.MediaColumns.RELATIVE_PATH, relativePath)
            }

        }

    }

    fun Context.createMediaOutputStoreOptions(contentValues: ContentValues): MediaStoreOutputOptions {
        return MediaStoreOutputOptions.Builder(
            applicationContext.contentResolver,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
            .setContentValues(contentValues)
            .build()
    }

    fun mergeTwoVideoStreams(video1: Uri, video2: Uri, outputUri: Uri) {
        //Get the two streams as media extractors
        val extractor1 = MediaExtractor()
        extractor1.setDataSource(video1.path!!)
        val extractor2 = MediaExtractor()
        extractor2.setDataSource(video2.path!!)

        // Get track info for each extractor object
        val numTracks1 = extractor1.trackCount
        val numTracks2 = extractor2.trackCount

        // Create a Media muxer and set the output Uri to it
        val mediaMuxer = MediaMuxer(outputUri.path!!, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4)

        // Add the tracks from the two MediaExtractor objects to the MediaMuxer object.
        for (i in 0 until numTracks1) {
            val format1 = extractor1.getTrackFormat(i)
            val trackIndex = mediaMuxer.addTrack(format1)
            extractor1.selectTrack(trackIndex)
            mediaMuxer.start()
            while (true) {
                val sampleData = ByteBuffer.allocate(1024)
                val sampleSize = extractor1.readSampleData(sampleData, 0)
                if (sampleSize < 0) {
                    break
                }
                val bufferInfo = MediaCodec.BufferInfo()
                bufferInfo.size = sampleSize
                bufferInfo.presentationTimeUs = extractor1.sampleTime
                mediaMuxer.writeSampleData(trackIndex, sampleData, bufferInfo)
                extractor1.advance()
                //if (extractor1.sampleTime >= extractor1.)
            }
        }

        for (i in 0 until numTracks2) {
            val format2 = extractor2.getTrackFormat(i)
            val trackIndex = mediaMuxer.addTrack(format2)
            extractor2.selectTrack(trackIndex)
            mediaMuxer.start()
            while (true) {
                val sampleData = ByteBuffer.allocate(1024)
                val sampleSize = extractor2.readSampleData(sampleData, 0)
                if (sampleSize < 0) {
                    break
                }
                val bufferInfo = MediaCodec.BufferInfo().apply {
                    size = sampleSize
                    presentationTimeUs = extractor2.sampleTime
                }
                mediaMuxer.writeSampleData(trackIndex, sampleData, bufferInfo)
            }
        }
    }
}