package com.ngengeapps.cameraxnew

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ConcurrentCamera.SingleCameraConfig
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCapture.OutputFileOptions
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.core.UseCaseGroup
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.video.PendingRecording
import androidx.camera.video.Recorder
import androidx.camera.video.Recording
import androidx.camera.video.VideoCapture
import androidx.camera.video.VideoRecordEvent
import androidx.concurrent.futures.await
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.ngengeapps.cameraxnew.Utils.createMediaOutputStoreOptions
import com.ngengeapps.cameraxnew.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private var backCameraSelector: CameraSelector? = null
    private var frontCameraSelector: CameraSelector? = null
    private val backRecorder: Recorder by lazy { Recorder.Builder().build() }
    private val frontRecorder: Recorder by lazy { Recorder.Builder().build() }

    private val frontVideoCapture: VideoCapture<Recorder> by lazy {
        VideoCapture.withOutput(frontRecorder)
    }
    private val backVideoCapture: VideoCapture<Recorder> by lazy {
        VideoCapture.withOutput(backRecorder)
    }

    private var frontRecording: Recording? = null
    private var backRecording: Recording? = null

    private val frontImageCapture: ImageCapture by lazy {
        ImageCapture.Builder()
            .build()
    }


    private val backImageCapture: ImageCapture by lazy {
        ImageCapture.Builder()
            .build()
    }


    private val mainBinding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    private val supportsConcurrentCamera: Boolean by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_CONCURRENT)
        } else {
            false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mainBinding.root)
        if (supportsConcurrentCamera) {
            lifecycleScope.launch {
                startDualCameras()
            }
        } else {
            //Toast.makeText(this, "This phone does not support concurrent cameras", Toast.LENGTH_SHORT).show()
            Toast.makeText(this, "onCreate: Phone does not support dual camera", Toast.LENGTH_SHORT)
                .show()
            mainBinding.buttonCapture.isEnabled = false
        }

        mainBinding.buttonCapture.setOnClickListener {
            recordFrontAndBackVideos()
        }
    }


    private suspend fun startDualCameras() {
        val cameraProvider = ProcessCameraProvider.getInstance(this).await()

        for (cameraInfos in cameraProvider.availableConcurrentCameraInfos) {
            Log.d("MainActivity", "startDualCameras: ${cameraInfos.first()}")
            backCameraSelector = cameraInfos.firstOrNull {
                it.lensFacing == CameraSelector.LENS_FACING_BACK
            }?.cameraSelector

            frontCameraSelector = cameraInfos.firstOrNull {
                it.lensFacing == CameraSelector.LENS_FACING_FRONT
            }?.cameraSelector

            if (backCameraSelector == null || frontCameraSelector == null) {
                // If either a primary or secondary selector wasn't found, reset both
                // to move on to the next list of CameraInfos.
                backCameraSelector = null
                frontCameraSelector = null
            } else {
                //Conclude the search both primary and secondary cameras are found
                break
            }
        }

        if (backCameraSelector == null || frontCameraSelector == null) {
            return
        }
        val frontPreview = Preview.Builder()
            .build()
            .also { it.setSurfaceProvider(mainBinding.frontPreviewView.surfaceProvider) }
        val backPreview = Preview
            .Builder()
            .build()
            .also { it.setSurfaceProvider(mainBinding.backPreviewView.surfaceProvider) }

        val primary = SingleCameraConfig(
            backCameraSelector!!,
            UseCaseGroup.Builder()
                .addUseCase(backPreview)
                //.addUseCase(backImageCapture)
                .addUseCase(backVideoCapture)
                .build(),
            this
        )

        val secondary = SingleCameraConfig(
            frontCameraSelector!!,
            UseCaseGroup.Builder().addUseCase(frontPreview)
                //.addUseCase(frontImageCapture)
                .addUseCase(frontPreview)
                .addUseCase(frontVideoCapture)
                .build(), this
        )

        val concurrentCamera = cameraProvider.bindToLifecycle(listOf(primary, secondary))
    }

    private fun takeDualPhotos() {
        val backOutputFileOptions = OutputFileOptions.Builder(
            contentResolver,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            Utils.createContentValues(suffix = "back")
        ).build()

        val frontOutputFileOptions = OutputFileOptions.Builder(
            contentResolver,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            Utils.createContentValues(suffix = "front")
        ).build()
        backImageCapture.takePicture(backOutputFileOptions, ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    runOnUiThread {
                        Toast.makeText(
                            this@MainActivity,
                            "Captured image successfully with Uri ${outputFileResults.savedUri}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onError(exception: ImageCaptureException) {
                    runOnUiThread {
                        Toast.makeText(
                            this@MainActivity,
                            "Error capturing photo ${exception.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        frontImageCapture.takePicture(frontOutputFileOptions, ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    runOnUiThread {
                        Toast.makeText(
                            this@MainActivity,
                            "Captured front image successfully with Uri ${outputFileResults.savedUri}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onError(exception: ImageCaptureException) {
                    runOnUiThread {
                        Toast.makeText(
                            this@MainActivity,
                            "Error capturing photo ${exception.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

    }


    @SuppressLint("MissingPermission")
    private fun recordFrontAndBackVideos() {
        val currentFrontRecording = frontRecording
        val currentBackRecording = backRecording
        if (currentFrontRecording == null && currentBackRecording == null) {
            val pendingFrontRecording: PendingRecording =
                frontVideoCapture.output.prepareRecording(
                    this,
                    createMediaOutputStoreOptions(
                        Utils.createContentValues(
                            isImage = false,
                            suffix = "front"
                        )
                    )
                ).withAudioEnabled()
            val pendingBackRecording: PendingRecording =
                backVideoCapture.output.prepareRecording(
                    this,
                    createMediaOutputStoreOptions(
                        Utils.createContentValues(
                            isImage = false,
                            suffix = "back"
                        )
                    )
                ).withAudioEnabled()
            frontRecording =
                pendingFrontRecording.start(ContextCompat.getMainExecutor(this)) { recordEvent ->
                    when (recordEvent) {
                        is VideoRecordEvent.Start -> {
                            mainBinding.buttonCapture.text = getString(R.string.stop_record)
                        }

                        is VideoRecordEvent.Resume -> {
                            //updateUi(VideoRecorderUiState.Resumed)
                        }

                        is VideoRecordEvent.Pause -> {
                            //updateUi(VideoRecorderUiState.Paused)
                        }


                        is VideoRecordEvent.Finalize -> {
                            mainBinding.buttonCapture.text = getString(R.string.start_record)
                            if (!recordEvent.hasError()) {
                                val uri: Uri = recordEvent.outputResults.outputUri
                                Toast.makeText(
                                    this,
                                    "Recorded front successfully",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {

                                frontRecording?.close()
                                frontRecording = null
                                Log.e(
                                    "Main", "Video capture ends with error: " +
                                            "${recordEvent.error}"
                                )
                            }
                        }
                    }

                }

            backRecording =
                pendingBackRecording.start(ContextCompat.getMainExecutor(this)) { recordEvent ->
                    when (recordEvent) {
                        is VideoRecordEvent.Start -> {
                            mainBinding.buttonCapture.text = getString(R.string.stop_record)
                        }

                        is VideoRecordEvent.Resume -> {
                            //updateUi(VideoRecorderUiState.Resumed)
                        }

                        is VideoRecordEvent.Pause -> {
                            //updateUi(VideoRecorderUiState.Paused)
                        }


                        is VideoRecordEvent.Finalize -> {
                            mainBinding.buttonCapture.text = getString(R.string.start_record)
                            if (!recordEvent.hasError()) {
                                val uri: Uri = recordEvent.outputResults.outputUri
                                Toast.makeText(
                                    this,
                                    "Recorded back successfully:Uri:$uri",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {

                                backRecording?.close()
                                backRecording = null
                                Log.e(
                                    "Main", "Video capture ends with error: " +
                                            "${recordEvent.error}"
                                )
                            }
                        }
                    }

                }

        } else {
            currentFrontRecording?.stop()
            currentBackRecording?.stop()
            backRecording = null
            frontRecording = null
        }

    }
}