package com.ngengeapps.cameraxnew

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat


class PermissionsActivity : AppCompatActivity() {

    // Define the permissions we need
    private val requiredPermissions = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO
    )

    // Register for activity result using the new API
    private val requestMultiplePermissions =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            // Check if all permissions are granted
            val allPermissionsGranted = permissions.all { it.value }
            if (allPermissionsGranted) {
                // If all permissions are granted, navigate to MainActivity
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                // If any of the permissions are denied, show a rationale or handle the denial
                showPermissionDeniedRationale()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permissions)

        // Check permissions on app start
        checkPermissions()
    }

    private fun checkPermissions() {
        // Check which permissions are already granted
        val deniedPermissions = requiredPermissions.filter {
            ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
        }

        if (deniedPermissions.isEmpty()) {
            // If all permissions are already granted, navigate to MainActivity
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            // If any permissions are denied, request them
            requestMultiplePermissions.launch(deniedPermissions.toTypedArray())
        }
    }

    private fun showPermissionDeniedRationale() {
        // This is where you can show a message to the user explaining why the permissions are necessary
        // You can use a Dialog, Snackbar, or any other way to inform the user
        // For this example, I'll just show a simple toast
        Toast.makeText(this, "All permissions are required to use this app.", Toast.LENGTH_LONG)
            .show()

        // Optionally, you can give the user another chance to grant permissions
        checkPermissions()
    }
}
