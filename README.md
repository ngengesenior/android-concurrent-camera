## CameraXNew

This is a demo project for new CameraX features for previewing front and back Camera simultaneously
as well as
capturing images from both cameras simultaneously, recording video from both cameras simultaneously.

### Notes

- Using ImageCapture UseCase and Video UseCase in the SingleCameraConfig at the same time crashes
  the app, so you have to choose which one to use.
- Dual Cameras for CameraX work from Android 11 and above and not all devices are supported
- The app is tested on Pixel 7a and 3a. It fails on 3a and works on 7a
- This is WIP

Learn
more [About CameraX Concurrent Cameras](https://android-developers.googleblog.com/2023/06/camerax-13-is-now-in-beta.html)
